package com.example.chrtak003.starwars;

import android.util.Log;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.utils.URLEncodedUtils;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.protocol.BasicHttpContext;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.SocketTimeoutException;
import java.util.List;

public class JSONParser {

    public static String jsonDirections;
    public static String jsonAddress;
    static InputStream is = null;
    static JSONObject jObj = null;
    static String json = "";

    // constructor
    public JSONParser() {

    }

    public String getDirections()
    {
        return jsonDirections;
    }

    public String getAddress()
    {
        return jsonAddress;
    }


    public JSONObject getJSONFromUrl(final String url) {

        // Making HTTP request
        try {
            // Construct the client and the HTTP request.
            DefaultHttpClient httpClient = new DefaultHttpClient();
            HttpPost httpPost = new HttpPost(url);

            // Execute the POST request and store the response locally.
            HttpResponse httpResponse = httpClient.execute(httpPost);
            // Extract data from the response.
            HttpEntity httpEntity = httpResponse.getEntity();
            // Open an inputStream with the data content.
            is = httpEntity.getContent();

        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (ClientProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        try {
            // Create a BufferedReader to parse through the inputStream.
            BufferedReader reader = new BufferedReader(new InputStreamReader(
                    is, "iso-8859-1"), 8);
            // Declare a string builder to help with the parsing.
            StringBuilder sb = new StringBuilder();
            // Declare a string to store the JSON object data in string form.
            String line = null;

            // Build the string until null.
            while ((line = reader.readLine()) != null) {
                sb.append(line + "\n");
            }

            // Close the input stream.
            is.close();
            // Convert the string builder data to an actual string.
            json = sb.toString();

            jsonDirections = json;
            jsonAddress = json;
            System.out.println(json);
        } catch (Exception e) {
            Log.e("Buffer Error", "Error converting result " + e.toString());
        }

        // Try to parse the string to a JSON object
        try {
            jObj = new JSONObject(json);
        } catch (JSONException e) {
            Log.e("JSON Parser", "Error parsing data " + e.toString());
        }

        // Return the JSON Object.
        return jObj;

    }


    // function get json from url
    public JSONObject makeHttpRequest(String url, String method,List<NameValuePair> params) {

        // Making HTTP request
        try {

            HttpParams httpParameters = new BasicHttpParams();
            int timeoutConnection = 3000;
            HttpConnectionParams.setConnectionTimeout(httpParameters, timeoutConnection);
            int timeoutSocket = 3000;
            HttpConnectionParams.setSoTimeout(httpParameters, timeoutSocket);

            // check for request method
            if(method == "POST"){

                Log.d("inParser", "starting");

                DefaultHttpClient httpClient = new DefaultHttpClient(httpParameters);

                Log.d("check1", "parser");

                HttpPost httpPost = new HttpPost(url);

                Log.d("check2", "parser");
                httpPost.setEntity(new UrlEncodedFormEntity(params));

                Log.d("check3", "parser");
                HttpResponse httpResponse = httpClient.execute(httpPost, new BasicHttpContext());

                Log.d("check4", "parser");
                HttpEntity httpEntity = httpResponse.getEntity();
                Log.d("check5", "parser");
                is = httpEntity.getContent();
                Log.d("check6", "parser");

            }else if(method == "GET"){
                // request method is GET
                DefaultHttpClient httpClient = new DefaultHttpClient(httpParameters);

                String paramString = URLEncodedUtils.format(params, "utf-8");
                url += "?" + paramString;
                HttpGet httpGet = new HttpGet(url);

                HttpResponse httpResponse = httpClient.execute(httpGet);
                HttpEntity httpEntity = httpResponse.getEntity();
                is = httpEntity.getContent();
            }

        } catch (UnsupportedEncodingException e) {
            System.out.println(" !!!!!!!! Unsupported encoding");
            e.printStackTrace();
            return null;
        } catch (ClientProtocolException e) {
            System.out.println(" !!!!!!!! Client Protocol");
            e.printStackTrace();
            return null;
        } catch(SocketTimeoutException e){
            System.out.println(" !!!!!!!! Time out exception");
            e.printStackTrace();
            return null;
        }catch (IOException e) {
            System.out.println(" !!!!!!!! IOException");
            //e.printStackTrace();
            return null;
        }

        try {
            BufferedReader reader = new BufferedReader(new InputStreamReader(
                    is, "iso-8859-1"), 8);
            Log.d("check7", "parser");
            StringBuilder sb = new StringBuilder();
            String line = null;
            while ((line = reader.readLine()) != null) {
                sb.append(line + "\n");
            }
            is.close();
            Log.d("check8", "parser");

            json = sb.toString();
            json = removeNotices(json);


            System.out.println("trying to print json: "+json +"  *******");
            Log.d("check9", "parser");
        } catch (Exception e) {
            Log.e("Buffer Error", "Error converting result " + e.toString());
        }

        // try parse the string to a JSON object
        try {
            System.out.println(json);

            jObj = new JSONObject(json);
            Log.d("check11", "parser");
        } catch (JSONException e) {
            Log.e("JSON Parser", "Error parsing data " + e.toString());
        }

        // return JSON String
        return jObj;

    }

    public String removeNotices(String json)
    {
        int indexStart = json.indexOf("<br />");
        int indexEnd = json.lastIndexOf("<br />");

        System.out.println("edited json: "+json+ " start "+indexStart+" end "+indexEnd);
        return json;

    }
}