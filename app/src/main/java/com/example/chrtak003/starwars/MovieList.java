package com.example.chrtak003.starwars;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.LinkedHashMap;

/**
 * Created by chrtak003 on 2016/11/20.
 *
 * Custom ArrayAdapter to enable images for each list item
 */

public class MovieList extends ArrayAdapter<String>{

    private final Activity context;
    private final ArrayList<String> web;
    private final LinkedHashMap<String, Film> films;

    public MovieList(Activity context, ArrayList<String>  web,LinkedHashMap<String, Film> films) {

        super(context, R.layout.movie_listview, web);
        this.context = context;
        this.web = web;
        this.films = films;

    }

    @Override
    public View getView(int position, View view, ViewGroup parent) {

        LayoutInflater inflater = context.getLayoutInflater();
        View rowView= inflater.inflate(R.layout.movie_listview, null, true);
        TextView txtTitle = (TextView) rowView.findViewById(R.id.txt);

        ImageView imageView = (ImageView) rowView.findViewById(R.id.img);
        txtTitle.setText(web.get(position));

        switch (films.get(web.get(position)).getEpisodeId()){
            case 1:
                imageView.setImageResource(R.drawable.a_new_hope);
                break;
            case 2:
                imageView.setImageResource(R.drawable.the_empire_strikes_back);
                break;
            case 3:
                imageView.setImageResource(R.drawable.return_of_the_jedi);
                break;
            case 4:
                imageView.setImageResource(R.drawable.the_phantom_menace);
                break;
            case 5:
                imageView.setImageResource(R.drawable.attack_of_the_clones);
                break;
            case 6:
                imageView.setImageResource(R.drawable.revenge_of_the_sith);
                break;
            case 7:
                imageView.setImageResource(R.drawable.the_force_awakens);
                break;
        }

        return rowView;
    }
}