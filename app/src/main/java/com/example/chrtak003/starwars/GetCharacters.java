package com.example.chrtak003.starwars;

import android.os.AsyncTask;

import org.apache.http.NameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by chrtak003 on 2016/11/20.
 *
 * This class gets a single character information and calls the callback method on completion
 */
public class GetCharacters extends AsyncTask<Void, Void, HashMap<String,Character>> {

    private String url = "http://swapi.co/api/people/";

    private IGetData gettingData;

    private HashMap<String,Character> characterInformation = new HashMap<String,Character>();

    private JSONParser allCharacters = new JSONParser();

    private JSONArray characters = null;

    public GetCharacters(IGetData iGetData){
        this.gettingData = iGetData;
    }

    public void setCharacterUrl(String url){
        this.url=url;
    }

    @Override
    protected HashMap<String,Character> doInBackground(Void... arg0) {

        try{
            List<NameValuePair> allCharactersParams = new ArrayList<NameValuePair>();

            JSONObject c = allCharacters.makeHttpRequest(url, "GET", allCharactersParams);

            if (c == null){
                return null;
            }

            System.out.println("character: " + c.getString("name"));
            Character character = new Character();

            character.setName(c.getString("name"));
            character.setBirthYear(c.getString("birth_year"));
            character.setEyeColor(c.getString("eye_color"));
            character.setGender(c.getString("gender"));
            character.setHairColor(c.getString("hair_color"));
            character.setHeight(c.getString("height"));
            character.setMass(c.getString("mass"));
            character.setSkinColor(c.getString("skin_color"));

            characterInformation.put(c.getString("name"),character);


        }catch (JSONException e) {
            e.printStackTrace();
            return null;
        }

        return characterInformation;
    }


    protected void onPostExecute(HashMap<String,Character> characters) {

        if (characters == null){
            System.out.println("characters are null");
        }else{
            gettingData.postGetCharacters(characters);
        }
    }
}
