package com.example.chrtak003.starwars;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by chrtak003 on 2016/11/20.
 *
 * Main application
 */

public class Movies extends AppCompatActivity implements IGetData {

    private static LinkedHashMap<String, Film> films;

    private ArrayList<String> movieList = new ArrayList<String>();

    private ArrayList<Integer> imageList = new ArrayList<Integer>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_movies);

        GetFilms getFilms = new GetFilms(this);
        getFilms.execute();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_movies, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            //return true;
        }

        return super.onOptionsItemSelected(item);
    }

    /**
     * Creates the adapter to be put in the list view
     * */
    public void createListAdapter(){

        for (Map.Entry<String,Film> e: films.entrySet()){
            Film film = e.getValue();

            movieList.add(film.getTitle());
            imageList.add(R.drawable.star_wars);

        }

        MovieList adapter = new MovieList(this, movieList,films);

        ListView listView = (ListView) findViewById(R.id.movies_list);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> arg0, View arg1, int position, long arg3) {
                Toast.makeText(Movies.this, "Showing " + movieList.get(position), Toast.LENGTH_SHORT).show();

                Intent i = new Intent(Movies.this,Movie.class);
                i.putExtra("film", films.get(movieList.get(position)));
                startActivity(i);
            }
        });

        listView.setAdapter(adapter);

    }

    /**
     * Sorts the linked hash map according to the release date
     * Earliest is in front of the map and latest at the back
     * */

    public LinkedHashMap<String,Film> sort(LinkedHashMap<String, Film> films){
        LinkedHashMap<String,Film> sorted = new LinkedHashMap<String,Film>();

        List<Map.Entry<String,Film>> entries = new ArrayList<>(films.entrySet());

        Collections.sort(entries, new Comparator<Map.Entry<String, Film>>() {
            @Override
            public int compare(Map.Entry<String,Film> lhs, Map.Entry<String,Film> rhs) {
                return lhs.getValue().getReleaseDate().compareTo(rhs.getValue().getReleaseDate());
            }
        });

        for (Map.Entry<String, Film> entry:entries){
            sorted.put(entry.getKey(),entry.getValue());
        }

        return sorted;
    }

    @Override
    public void postGetFilms(LinkedHashMap<String, Film> films) {
        this.films = sort(films);
        createListAdapter();
    }

    @Override
    public void postGetCharacters(HashMap<String, Character> characters) {

    }
}
