package com.example.chrtak003.starwars;

import android.animation.ObjectAnimator;
import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 * Created by chrtak003 on 2016/11/21.
 *
 * This is the class for showing the Movie Information
 */

public class Movie extends AppCompatActivity implements IGetData{

    private Film movie;
    private ImageView movieImage;
    private ImageButton toggle;
    private TextView movieTitle;
    private TextView releaseDate;
    private TextView openingCrawl;
    private LinearLayout scrollCharacters;
    private boolean expanded=false;
    private int openingCrawlLen;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.movie_info);

        Bundle b=this.getIntent().getExtras();
        movie=(Film)b.getSerializable("film");

        movieImage = (ImageView)this.findViewById(R.id.movie_picture);
        movieTitle = (TextView)this.findViewById(R.id.movie_title);
        releaseDate = (TextView)this.findViewById(R.id.release_date);
        openingCrawl = (TextView)this.findViewById(R.id.opening_crawl);
        scrollCharacters = (LinearLayout)this.findViewById(R.id.scroll_characters);

        toggle = (ImageButton)this.findViewById(R.id.toggle);
        toggle.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                if (!expanded){
                    expandTextView(openingCrawl);
                    expanded = true;
                }else{
                    collapseTextView(openingCrawl, 3);
                    expanded = false;
                }
            }
        });

        fillMovieInfo();
        getMovieCharacters();
    }

    /**
     * Populates the movie info layout view with the necessary details
     * */
    public void fillMovieInfo(){
        movieTitle.setText(movie.getTitle());
        releaseDate.setText("Release Date: " + movie.getFormattedReleaseDate());

        openingCrawlLen = movie.getOpeningCrawl().split(System.getProperty("line.separator")).length;
        openingCrawl.setMaxLines(openingCrawlLen);
        openingCrawl.setText(movie.getOpeningCrawl());
        collapseTextView(openingCrawl, 3);

        switch (movie.getEpisodeId()){
            case 1:
                movieImage.setImageResource(R.drawable.a_new_hope);
                break;
            case 2:
                movieImage.setImageResource(R.drawable.the_empire_strikes_back);
                break;
            case 3:
                movieImage.setImageResource(R.drawable.return_of_the_jedi);
                break;
            case 4:
                movieImage.setImageResource(R.drawable.the_phantom_menace);
                break;
            case 5:
                movieImage.setImageResource(R.drawable.attack_of_the_clones);
                break;
            case 6:
                movieImage.setImageResource(R.drawable.revenge_of_the_sith);
                break;
            case 7:
                movieImage.setImageResource(R.drawable.the_force_awakens);
                break;
        }
    }

    private void expandTextView(TextView tv){
        toggle.setImageResource(R.drawable.collapse);
        ObjectAnimator animation = ObjectAnimator.ofInt(tv, "maxLines", openingCrawlLen);
        animation.setDuration(400).start();
    }

    private void collapseTextView(TextView tv, int numLines){
        toggle.setImageResource(R.drawable.expand);
        ObjectAnimator animation = ObjectAnimator.ofInt(tv, "maxLines", numLines);
        animation.setDuration(400).start();
    }

    /**
     * Iterates through the character url list and gets information for each character
     * When obtained, postGetCharacters is called with the result
     * */
    public void getMovieCharacters(){

        ArrayList<String> characters = movie.getCharacters();

        for (String characterUrl:characters){

            GetCharacters getCharacters = new GetCharacters(this);
            getCharacters.setCharacterUrl(characterUrl);
            getCharacters.execute();

        }

    }

    /**
     * Adds the view to the horizontal scroll view for movie info layout
     * */
    public void insertCharacters(HashMap<String, Character>  characters){

        for (Map.Entry<String,Character> e:characters.entrySet()){
            Character character = e.getValue();

            LayoutInflater inflater = (LayoutInflater)this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            LinearLayout characterLayout = (LinearLayout)inflater.inflate(R.layout.character_info, null);

            TextView name = (TextView) characterLayout.findViewById(R.id.name);
            TextView height = (TextView) characterLayout.findViewById(R.id.height);
            TextView mass = (TextView) characterLayout.findViewById(R.id.mass);
            TextView skinColor = (TextView) characterLayout.findViewById(R.id.skin_color);
            TextView eyeColor = (TextView) characterLayout.findViewById(R.id.eye_color);
            TextView hairColor = (TextView) characterLayout.findViewById(R.id.hair_color);
            TextView birthYear = (TextView) characterLayout.findViewById(R.id.birth_year);
            TextView gender = (TextView) characterLayout.findViewById(R.id.gender);

            name.setText(character.getName());
            mass.setText("Mass: "+character.getMass());
            skinColor.setText("Skin Color: "+character.getSkinColor());
            eyeColor.setText("Eye Color: "+character.getEyeColor());
            hairColor.setText("Hair Color: "+character.getHairColor());
            birthYear.setText("Birth Year: "+character.getBirthYear());
            gender.setText("Gender: "+character.getGender());
            height.setText("Height: "+character.getHeight());

            scrollCharacters.addView(characterLayout);

        }

    }

    @Override
    public void postGetFilms(LinkedHashMap<String, Film> films) {

    }

    @Override
    public void postGetCharacters(HashMap<String, Character> characters) {
        insertCharacters(characters);
    }
}
