package com.example.chrtak003.starwars;

import android.os.AsyncTask;

import org.apache.http.NameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;

/**
 * Created by chrtak003 on 2016/11/20.
 *
 * This class gets a single film information and calls the callback method on completion
 */

public class GetFilms extends AsyncTask<Void, Void, LinkedHashMap<String,Film>> {

    private String url = "http://swapi.co/api/films/";

    private IGetData gettingData;

    private LinkedHashMap<String,Film> filmInformation = new LinkedHashMap<String,Film>();

    private JSONParser allFilms = new JSONParser();

    private JSONArray films = null;

    public GetFilms(IGetData gettingData){
        this.gettingData=gettingData;
    }

    @Override
    protected LinkedHashMap<String,Film> doInBackground(Void... arg0) {

        try{
            List<NameValuePair> allFilmsParams = new ArrayList<NameValuePair>();

            JSONObject allFilmsJson = allFilms.makeHttpRequest(url, "GET",allFilmsParams);

            if (allFilmsJson == null){
                return null;
            }

            films = allFilmsJson.getJSONArray("results");
            System.out.println("size: "+films.length());

            // looping through All FIlms
            for (int i = 0; i < films.length(); i++) {
                JSONObject c = films.getJSONObject(i);
                System.out.println("film: " + c.getString("title"));
                Film film = new Film();

                film.setTitle(c.getString("title"));
                JSONArray characters = c.getJSONArray("characters");

                ArrayList<String> list = new ArrayList<String>();
                for (int j=0; j<characters.length(); j++) {
                    list.add(characters.getString(j));
                }

                film.setCharacters(list);
                film.setCreated(c.getString("title"));
                film.setDirector(c.getString("director"));
                film.setEdited(c.getString("edited"));
                film.setOpeningCrawl(c.getString("opening_crawl"));
                film.setReleaseDate(c.getString("release_date"));
                film.setEpisodeId(c.getInt("episode_id"));

                filmInformation.put(c.getString("title"),film);

            }

        }catch (JSONException e) {
            e.printStackTrace();
            return null;
        }

        return filmInformation;
    }


    protected void onPostExecute(LinkedHashMap<String,Film> films) {

        if (films == null){
            System.out.println("films are null");
        }else{
            gettingData.postGetFilms(films);
        }
    }

}