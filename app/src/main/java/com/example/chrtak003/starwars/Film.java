package com.example.chrtak003.starwars;

import android.text.format.DateUtils;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

/**
 * Created by chrtak003 on 2016/11/20.
 */
public class Film implements Serializable {

    private ArrayList<String> characters = new ArrayList<String>();
    private String title;
    private String created;
    private String director;
    private String edited;
    private String openingCrawl;
    private String releaseDate;
    private String formattedReleaseDate;
    private int episodeId;

    public ArrayList<String> getCharacters() {
        return characters;
    }

    public void setCharacters(ArrayList<String> characters) {
        this.characters = characters;
    }

    public String getCreated() {
        return created;
    }

    public void setCreated(String created) {
        this.created = created;
    }

    public String getDirector() {
        return director;
    }

    public void setDirector(String director) {
        this.director = director;
    }

    public String getEdited() {
        return edited;
    }

    public void setEdited(String edited) {
        this.edited = edited;
    }

    public String getOpeningCrawl() {
        return openingCrawl;
    }

    public void setOpeningCrawl(String openingCrawl) {
        this.openingCrawl = openingCrawl;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getFormattedReleaseDate() {

        SimpleDateFormat ft = new SimpleDateFormat ("yyyy-MM-dd");
        Date date;

        if (formattedReleaseDate != null){
            return formattedReleaseDate;
        }

        try {
            date = ft.parse(releaseDate);
            long now = System.currentTimeMillis();
            formattedReleaseDate = (DateUtils.getRelativeTimeSpanString(date.getTime(), now, DateUtils.YEAR_IN_MILLIS)).toString();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return formattedReleaseDate;
    }

    public void setFormattedReleaseDate(String formattedReleaseDate) {
        this.formattedReleaseDate = formattedReleaseDate;
    }

    public int getEpisodeId() {
        return episodeId;
    }

    public void setEpisodeId(int episodeId) {
        this.episodeId = episodeId;
    }

    public String getReleaseDate() {
        return releaseDate;
    }

    public void setReleaseDate(String releaseDate) {
        this.releaseDate = releaseDate;
    }
}
