package com.example.chrtak003.starwars;

import java.util.HashMap;
import java.util.LinkedHashMap;

/**
 * Created by chrtak003 on 2016/11/20.
 */
public interface IGetData {

    public void postGetFilms(LinkedHashMap<String,Film> films);

    public void postGetCharacters(HashMap<String,Character> characters);

}
